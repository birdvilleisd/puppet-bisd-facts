Birdville Facts
======

##Overview

Some custom facts specific to Birdville ISD.

###building.rb

Hostnames at Birdville should all start with building number followed by a hyphen. This pulls the building code out for usage in puppet.  EX: 011-123-macabc; Building = 011

###mac_geektool_test.rb
This fact will check for the existence of /Applications/GeekTool.app and return true if the app is present on the filesystem.

###ipaddress_segment_matching.rb
This fact allows you to correlate IP subnets with a segment name. To modify it, simply change the `network_segments` hash and add a segment name and IP subnet/mask.

###room.rb

Hostnames at Birdville should have the room number in the second hyphenated section. This pulls the room number out for usage in puppet. Ex: 011-123-macabc; Room = 123

#More

https://github.com/grahamgilbert/grahamgilbert-mac_facts  
https://github.com/grahamgilbert/puppet-wan_ip